# encryptVault

#### installing


`go build -o ./encryptVault cmd/cli.go`


```
encryptVault is an API key and other secrets vault
 
 Usage:
   encryptVault [command]
 
 Available Commands:
   get         Gets a secret in your vault
   help        Help about any command
   set         Sets a secret in your vault
 
 Flags:
   -h, --help         help for encryptVault
   -k, --key string   The key to use when encoding and decoding secrets
 
 Use "encryptVault [command] --help" for more information about a command.
```

#### Example

Entering a key and secret
```
./encryptVault set twitter_api_token abc123
Value set successfully!
```

Getting a secret by key name

```
./encryptVault get twitter_api_token
twitter_api_token = abc123
```

#### About

 - The secret values are in a file located in your home directory called .secrets which is encrypted.
 - The crypto found in the cipher package is using crypto/cipher in the [standard library](https://pkg.go.dev/crypto/cipher)
 
 #### TODO:
 
  - Add `rm`, `list` features to command line
  - Add better handling for `~/.secrets` existing already in users `home` directory