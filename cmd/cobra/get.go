package cobra

import (
	"encryptvault"
	"fmt"
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use: "get",
	Short: "Gets a secret in your vault",
	Run: func(cmd *cobra.Command, args []string) {
		v := encryptvault.File(encodingKey, secretsPath())
		key := args[0]
		value, err := v.Get(key)
		if err != nil {
			fmt.Println("Key does not exist")
			return
		}
		fmt.Printf("%s = %s\n", key, value)
	},
}
func init() {
	RootCmd.AddCommand(getCmd)
}
