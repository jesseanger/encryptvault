package cobra

import (
	"github.com/spf13/cobra"
	"github.com/mitchellh/go-homedir"
	"path/filepath"
)

var RootCmd = &cobra.Command{
	Use: "encryptVault",
	Short: "encryptVault is an API key and other secrets vault",
}

var encodingKey string

func init() {
	RootCmd.PersistentFlags().StringVarP(&encodingKey, "key", "k", "", "The key to use when encoding and decoding secrets")
}

func secretsPath() string {
	home, _ := homedir.Dir()
	return filepath.Join(home, ".secrets")
}