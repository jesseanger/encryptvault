package main

import (
	"encryptvault/cmd/cobra"
)

func main() {
	cobra.RootCmd.Execute()
}